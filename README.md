**Description:**   
This is carrom game made solely using OpenGL and C++.   

**Setup:**   
To run the file on linux,
type:-   
1. make clean (removes the current executable file)   
2. make (creates the executable with the current changes to the file)   
3. ./carrom (run the carrom game)   