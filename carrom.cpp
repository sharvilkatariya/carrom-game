#include <iostream>
#include <cmath>
#include <sys/time.h>
#include <GL/glut.h>
#include <stdio.h>
#include "constants.h"
#include "carrom.h"
#include "board.h"
using namespace std;

#define PI 3.141592653589
#define DEG2RAD(deg) (deg * PI / 180)

// Function Declarations
void drawScene();
void drawScene2();
void update(int value);
void drawBox(float len);
void drawBall(float rad);
void drawPowerBar(float height, float width);
void initRendering();
void handleResize(int w, int h);
void handleKeypress1(unsigned char key, int x, int y);
void handleKeypress2(int key, int x, int y);
void handleMouseclick(int button, int state, int x, int y);

// Global Variables

board carrom_board(box_len);

int main(int argc, char **argv) {
    // Initialize GLUT
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    gettimeofday(&tv, 0);
    /*static */
    previous_time = tv.tv_sec + tv.tv_usec/1000000.0;
    int w = glutGet(GLUT_SCREEN_WIDTH);
    int h = glutGet(GLUT_SCREEN_HEIGHT);
    int windowWidth = w * 3 / 4;
    int windowHeight = h * 3 / 4;
    glutInitWindowSize(windowWidth, windowHeight);
    
    carrom_board.window_width = windowWidth;
    carrom_board.window_length = windowHeight;
    glutInitWindowPosition((w - windowWidth) / 2, (h - windowHeight) / 2);

    glutCreateWindow("Carrom 2D Game");  // Setup the window
    initRendering();
    
    // Register callbacks
    glutDisplayFunc(drawScene);
    glutIdleFunc(drawScene);
    glutKeyboardFunc(handleKeypress1);
    glutSpecialFunc(handleKeypress2);
    glutMouseFunc(handleMouseclick);
    glutReshapeFunc(handleResize);
    glutTimerFunc(1, update, 0);

    glutMainLoop();
    return 0;
}

void drawScene(){
    carrom_board.draw();
}

// Function to handle all calculations in the scene updated every 1 second
void update(int value){
    gettimeofday(&tv, 0);
    double current_time = tv.tv_sec + tv.tv_usec/1000000.0;
    if (current_time >= previous_time + 0.001){
        carrom_board.advance(coeff_friction, (current_time - previous_time)*500);
        previous_time = current_time;
    }
    glutTimerFunc(1, update, 0);
}

// Initializing some openGL 3D rendering options
void initRendering() {
    glEnable(GL_DEPTH_TEST);        // Enable objects to be drawn ahead/behind one another
    glEnable(GL_COLOR_MATERIAL);    // Enable coloring
    glEnable(GL_SMOOTH);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glEnable(GL_LIGHTING);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);   // Setting a background color
    //glClearColor( 0.7f, 0.3f, 1.0f, 0.0f );
}

// Function called when the window is resized
void handleResize(int w, int h) {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, (float)w / (float)h, 0.1f, 200.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void handleKeypress1(unsigned char key, int x, int y) {
    switch(key){
        case 27:
            carrom_board.print();
            exit(0);        // escape key is pressed
        case 'c':
        case 'C':
            carrom_board.change_striker_angle(-2.0f);
            break;
        case 'A':
        case 'a':
            carrom_board.change_striker_angle(2.0f);
            break;
        case 'r':
        case 'R':
            carrom_board.restart();
            break;
        case 'w':
        case 'W':
            if (coeff_friction<0.04){
                coeff_friction+= 0.01;
            }
            break;
        case 's':
        case 'S':
            if (coeff_friction>0.01){
                coeff_friction-= 0.01;
            }
            break;
        case 32:            //space key is pressed
            carrom_board.shoot();
            break;
    }
}
void handleKeypress2(int key, int x, int y) {
    switch(key){
        case GLUT_KEY_LEFT:
            carrom_board.move_striker_left();
            break;
        case GLUT_KEY_RIGHT:
            carrom_board.move_striker_right();
            break;
        case GLUT_KEY_UP:
            carrom_board.change_striker_power(0.1);
            break;
        case GLUT_KEY_DOWN:
            carrom_board.change_striker_power(-0.1);
            break;
    }
}

void handleMouseclick(int button, int state, int x, int y){
    if (button == GLUT_LEFT_BUTTON){
        carrom_board.handle_left_click(state, x, y);
    }
    else if (button == GLUT_RIGHT_BUTTON){
        if (state == GLUT_DOWN){
            carrom_board.check_board(x, y);
        }else if (state == GLUT_UP){
            carrom_board.move_striker(x, y);
        }
    }
}
