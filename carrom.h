#include <vector>
using namespace std;
#if !defined(MY_CARROM_LIB_H)
#define MY_CARROM_LIB_H 1

//**************Components******************************//
class Components{
public:
    double x, y;
    Components(double _x = 0, double _y = 0){
        x=_x;
        y=_y;
    }
    double getX(){
        return x;
    }
    double getY(){
        return y;
    }
    double getMagnitude(){
        return sqrt(x*x + y*y);
    }
    Components getUnitVector(){
        float magnitude = getMagnitude();
        return Components(x/magnitude, y/magnitude);
    }
    Components addX(Components, double);
    Components operator-(const Components &c2){
        return Components(this->x - c2.x, this->y - c2.y);
    }
};

Components Components::addX(Components comp, double _toadd){
    return Components(comp.x + _toadd, comp.y);
};

/***************************************************/

/*******************************************************/
class Components3d{
public:
    double x, y, z;
    Components3d(double _x=0.0, double _y=0.0, double _z=0.0){
        x=_x;
        y=_y;
        z=_z;
    }
    bool operator == (const Components3d &c2){
        return (this->x==c2.x && this->y == c2.y && this->z == c2.z);
    }
};

/*******************************************************/

//**************DISKS******************************//
class disc{
    bool state;
    int disc_no;

protected:
    double x, y, vel_x, vel_y;
    double radius, mass;
    Components3d color;
    
public:
    disc(int, float, float, Components, Components, Components3d);
    void setRadius(double );
    double getRadius(){
        return radius;
    };
    void setPosition(double ,double );
    void setVelocity(double ,double );
    void setVelocity(Components );
    Components getPosition();
    Components getVelocity();
    void draw();
    void setMass(double );
    double getMass(){
        return mass;
    };
    int getDiskno(){
        return disc_no;
    };
    bool advance(double, double);
    bool check_collision_with(disc&);
    void handle_collision(disc&, double, double);
    Components3d getColor(){
        return color;
    };
};

disc::disc(int diskno, float _mass, float _rad, Components position, Components velocity, Components3d _color)
{
    disc_no = diskno; 
    mass = _mass;
    radius = _rad;
    x = position.x;
    y = position.getY();
    vel_x = velocity.x;
    vel_y = velocity.getY();
    color = _color;
}
void disc::setPosition(double new_x, double new_y){
    x=new_x;
    y=new_y;
}

void disc::setVelocity(double new_velx, double new_vely){
    vel_x=new_velx;
    vel_y=new_vely;
}

void disc::setVelocity(Components new_velocity){
    vel_x = new_velocity.x;
    vel_y = new_velocity.y;
}

bool disc::check_collision_with(disc& disc_obj){
    Components position_coords = disc_obj.getPosition();
    if (fabs(radius + disc_obj.radius) < fabs(x - position_coords.x) + fabs(y - position_coords.y)){
        return false;
    }
    return true;
}

bool disc::advance(double coeff_fric=0.02, double dt=0){
    double magnitude = Components(vel_x, vel_y).getMagnitude();
    if (mass * magnitude < 0.05){
        setVelocity(0, 0);
        return false; //Not moving any longer.
    }
    vel_x -= vel_x/magnitude * (coeff_fric);
    vel_y -= vel_y/magnitude * (coeff_fric);

    x += vel_x*(dt/1000.0);
    y += vel_y*(dt/1000.0);
    return true;
}

void disc::handle_collision(disc& disc_obj, double coeff_friction, double dt)
{
    //Undo the Advancement that was set
    advance(coeff_friction, -dt);
    disc_obj.advance(coeff_friction, -dt);

    double vector_par[2], vector_per[2], u_par[2], v_per[2], v_par[2];
    Components position_coords = disc_obj.getPosition(), velocity_coords = disc_obj.getVelocity();
    double vel2_x = velocity_coords.x, vel2_y = velocity_coords.y, mass2 = disc_obj.getMass();
    Components velocity_unit = (this->getPosition() - position_coords).getUnitVector();
    
    vector_par[0] = velocity_unit.x;
    vector_par[1] = velocity_unit.y;

    vector_per[0] = velocity_unit.y;
    vector_per[1] = -velocity_unit.x;
    
    u_par[0]= vel_x * vector_par[0] + vel_y * vector_par[1];
    u_par[1]= vel2_x * vector_par[0] + vel2_y * vector_par[1];
    
    v_per[0]= vel_x * vector_per[0] + vel_y * vector_per[1];
    v_per[1]= vel2_x * vector_per[0] + vel2_y * vector_per[1];
    
    v_par[0] = (mass * u_par[0] + mass2 * u_par[1] + mass2 * coeff_restitution * (u_par[1]-u_par[0]))/(mass+mass2);
    v_par[1] = (mass * u_par[0] + mass2 * u_par[1] - mass * coeff_restitution * (u_par[1]-u_par[0]))/(mass+mass2);
    
    vel_x = v_par[0] * vector_par[0] + v_per[0] * vector_per[0];
    vel_y = v_par[0] * vector_par[1] + v_per[0] * vector_per[1];
    
    disc_obj.setVelocity(v_par[1] * vector_par[0] + v_per[1] * vector_per[0], 
        v_par[1] * vector_par[1] + v_per[1] * vector_per[1]);

    advance(coeff_friction, dt);
    disc_obj.advance(coeff_friction, dt);
}

Components disc::getPosition(){
    return Components(x, y);
}

Components disc::getVelocity(){
    return Components(vel_x, vel_y);
}

void disc::setRadius(double new_radius){
    radius = new_radius;
}

void disc::setMass(double new_mass){
    mass = new_mass;
}

void disc::draw(){
    //glBindTexture(GL_TEXTURE_2D, disc_texture);
    glColor3f(color.x, color.y, color.z);
    glPushMatrix();
        glTranslatef(x, y, 0.0f);
        glBegin(GL_TRIANGLE_FAN);
        for(int i=0 ; i<360 ; i++){
            glVertex2f(radius * cos(DEG2RAD(i)), radius * sin(DEG2RAD(i)));
        }
        glEnd();
    glPopMatrix();
}
/***************************************************/


//**************STRIKER******************************//
class Striker:public disc{
    double power; //Striker Power
    double angle; //Striker Angle
    int state;
public:
    Striker(double = 5.0, double = 1.0f, Components = Components(0, 0), Components = Components(0, 0), Components3d =Components3d(0, 0, 0));
    void setPositionX(double new_x){
        x = new_x;
    }
    void add_power(double _add){
        if (power < 1.0f) {
            power += _add;
        }
    }
    void sub_power(double _sub){
        power -= _sub;
        if (power <  + 0.0f){
            power = 0.0f;
        }
    }

    void setPosition(double ,double );

    double get_angle(){
        return angle;
    }

    double get_power(){
        return power;
    }

    void setPower(double _power){
        power = _power;
    }
    void change_angle(double change_by){
        angle += change_by;
        if (angle>=360.0f){
            angle -= 360.0f;
        }else if (angle<0.0f){
            angle += 360.0f;
        }
    }
    void setAngle(double _angle){
        angle = _angle;
    }

    void change_state(int new_state){
        state = new_state; //Decide if in Drag State or not.
    }
    int getState(){
        return state;
    }

};

Striker::Striker(double _mass, double _radius, Components position, Components velocity, Components3d _color):disc(0, _mass, _radius, position, velocity, _color)
{
    power = 0.1f;
    angle = 90.0f;
    state = 0; //Not on Drag State
    color = _color;
}

void Striker::setPosition(double new_x,double new_y){
    x=new_x;
    y=new_y;
    state = 0;
};

/***************************************************/



//**************POTS******************************//
class pot{
    vector<disc> discs;
    int PotNumber;
    float radius;
    Components position; //Coordinates of Pot Center
public:
    pot(int pot_number, float _radius, double center_x, double center_y): position(center_x, center_y)
    {
        PotNumber = pot_number;
        radius = _radius;
    };
    void add(disc disc_obj){
        discs.push_back(disc_obj);
    };
    void draw();
    int getPotNumber(){
        return PotNumber;
    };
    float getRadius(){
        return radius;
    };
    Components getPosition(){
        return Components(position.x, position.y);
    };
    void print(){
        int disk_no;
        for (unsigned int i=0; i<discs.size();i++)
        {
            disk_no = discs[i].getDiskno(); 
            switch(disk_no){
                case 1: cout<<"Queen\n";
                        break;
                case 0: cout<<"Striker\n";
                        break;
                default:
                    switch(disk_no%2){
                        case 0: 
                            cout<<"Green Carrom Men\n";
                            break;
                        default:
                            cout<<"Blue Carrom Men\n";
                    }
            }
        }
    }
};

void pot::draw(){
    glColor3f(0.5, 1, 0.5);
    glPushMatrix();
        glTranslatef(position.x, position.getY(), 0.0f);
        glBegin(GL_LINE_LOOP);
        for(int i=0 ; i<360 ; i++){
            glVertex2f(radius * cos(DEG2RAD(i)), radius * sin(DEG2RAD(i)));
        }
        glEnd();
    glPopMatrix();
}
/***************************************************/

/**************PLAYERS******************************/
class Player{
    Components3d color;
    int player_no;
    vector<disc> potted_discs;
    int score;
public:
    double angle; //Last shot Angle
    double power; //Last shot Power
    Player(Components3d = Components3d(0, 1, 0), int = 0);
    void add_disc(disc );
    int getScore();
    void setScore(double);
};

Player::Player(Components3d _color, int _player_no){
    player_no = _player_no;
    score = 0;
    color = _color;
    angle = 270;
    power = 0.1f;
}
void Player::add_disc(disc disc_obj){
    potted_discs.push_back(disc_obj);
    for(int i=0;i<type_of_carrom_men; i++){
        if (disc_obj.getColor() == Components3d(colors[i][0], colors[i][1], colors[i][2])){
            score += carrom_men_score[i];
        }
    }
    //score += 1;
}
int Player::getScore(){
    return score;
}
void Player::setScore(double new_score){
    score = new_score;
}

/***************************************************/


/**************SCOREBOARD******************************/

class Scoreboard{
//    void compute();
public:
    void draw(int, int);
};

void Scoreboard::draw(int score, int turn){
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0.0, glutGet(GLUT_SCREEN_WIDTH) * 3 / 4, 0.0, glutGet(GLUT_SCREEN_HEIGHT) * 3 / 4);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPushMatrix();
    glColor3f(0.0, 0.0, 1.0); 
    glRasterPos2i(box_len/2, box_len/2);
    string s = "Player " + std::to_string(turn) + " Score: " + std::to_string(score);
    void * font = GLUT_BITMAP_HELVETICA_18;
    for (string::iterator i = s.begin(); i != s.end(); ++i)
    {
        glutBitmapCharacter(font, *i);
    }
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
}
/******************************************************/

#endif
