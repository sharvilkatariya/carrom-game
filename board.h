#pragma once
#include <vector>
#include <GL/glut.h>
#if !defined(MY_CARROM_LIB_H)
#include <carrom.h>
#endif
#if !defined(MYLIB_CONSTANTS_H)
#define MYLIB_CONSTANTS_H 1

#include "constants.h"

#endif

//enum board_state;

class board
{
    vector<pot> pots;
    Striker striker;
    int turn;
    Scoreboard scorecard;
    //static const int MAX_POWER = 20;
    
    //State variables
    std::vector<disc> discs; //Active Disks
    //vector<disc> potted_discs; //Potted Disks
    /*List of Players playing the Game*/
    std::vector<Player> players;  

    enum board_state{ANIMATING, IDLE};
    board_state state;
    double box_len;  //Carrom Board Side Length
    
    //function to reset all carrom men positions
    void reset_board();

    //create pots
    void create_pots();

    void add_disk(disc new_disk){
        discs.insert(discs.begin(), new_disk);
    };

    bool remove_disks(unsigned int);
    void drawPowerBar(double height, double width);
    void handle_collisions(double, double);
    void wall_collisions(double);
    void disc_collisions(double, double);
    int check_sink(disc);
    void delete_players();
    void DrawBox(double radius);
    void drawStrikerLines(double, double);
public:
    //Constructor to set board's striker, disks and pots
    board(double);
    int window_width, window_length; 
    
    void check_board(int, int);
    
    //function to advance animation by delta t(in ms)
    void advance(double, double);

    float get_striker_angle(){
        return striker.get_angle();
    };

    void change_striker_angle(double change_by){
        striker.change_angle(change_by);
    }

    //Function to be called on mouse click/release
    void mouse_click(int, int, int, int);

    //function to draw board
    void draw();

    void shoot();

    //function to add disc to pot
    void pocket_disk(disc toSink, int potNumber);

    void move_striker_left();
    
    void move_striker_right();

    void move_striker(int x, int y);
    
    void change_striker_power(double change){
        if (state == IDLE){
            if (change > 0.0){
                striker.add_power(change);
            }else{
                striker.sub_power(-1*change);
            }
        }
    }

    void print();

    void restart();

    void handle_left_click(int state, int x, int y);
    //Default destructor
    //~board(void);
};

bool board::remove_disks(unsigned int index){
    if(index >= discs.size())
        return false;
    discs.erase(discs.begin()+index);
    return true;
}

void board::delete_players(){
    unsigned int size = players.size();
    if (size){
        players.erase(players.begin(), players.begin() + size);        
    }
}
board::board(double _board_length){
    state = IDLE;
    turn = 1;
    box_len = _board_length;
    reset_board();
    create_pots();
}
void board::restart(){
    double player_scores[no_of_players];
    for (int i=0; i<no_of_players; i++){
        player_scores[i] = players[i].getScore();
    }
    reset_board();
    for (int i=0; i<no_of_players; i++){
        players[i].setScore(player_scores[i]);
    }
    state = IDLE;
    turn = 1;
    striker.draw();
}

void board::reset_board(){
    while(remove_disks(0)==true); //delete disks vector array.
    /*Adding Striker to board Class*/
    delete_players();
    std::vector<std::vector<double> > player_colors(no_of_players+2, std::vector<double> (3)); //Player Colours + Striker + Queen Color
    for (int j = 0; j <= no_of_players+1; ++j)
    {
        player_colors[j].assign(colors[j], colors[j] + 3);
    }
    striker = Striker(mass_striker, striker_rad, Components(0.0f, -box_len/3), Components(0.0f, 0.0f), Components3d(player_colors[no_of_players+1][0], 
        player_colors[no_of_players+1][1], player_colors[no_of_players+1][2]));
    
    for (int j=0; j<no_of_players; j++){
        players.push_back(Player(Components3d(player_colors[j][0], 
            player_colors[j][1], player_colors[j][2]), j+1));
    }

    int i=1;
    /*Adding other carrom men*/
    disc carrom_men(i++, mass_carrom_men, ball_rad, Components(0.0f, 0.0f), Components(0, 0), Components3d(player_colors[no_of_players][0], 
        player_colors[no_of_players][1], player_colors[no_of_players][2]));    
    //disc carrom_men(mass_carrom_men, ball_rad, Components(0.0f, 0.0f), Components(0, 0));    
    add_disk(carrom_men);
    
    for(double angle = PI/12; angle <= 2*PI+0.2; angle+= PI/3, i++)
    {
        disc carrom_men2 = disc(i, mass_carrom_men, ball_rad, Components((2*ball_rad + 0.03)*cos(angle), (2*ball_rad + 0.03)*sin(angle)), Components(0, 0), 
            Components3d(player_colors[i&1][0], player_colors[i&1][1], player_colors[i&1][2])); //Adding different colors
        add_disk(carrom_men2);
    }
}

void board::create_pots(){
    pot pot1(1, pot_rad, -box_len/2 + pot_rad + 0.01, box_len/2 - pot_rad - 0.01); 
    //Get Coordinates of Center of Circle of the Pot(Top Left)
    pot pot2(2, pot_rad, box_len/2 - pot_rad - 0.01, box_len/2 - pot_rad - 0.01); 
    //Get Coordinates of Center of Circle of the Pot(Top Right)
    pot pot3(3, pot_rad, -box_len/2 + pot_rad + 0.01, -box_len/2 + pot_rad + 0.01); 
    //Get Coordinates of Center of Circle of the Pot(Bottom Left)
    pot pot4(4, pot_rad, box_len/2 - pot_rad - 0.01, -box_len/2 + pot_rad + 0.01); 
    //Get Coordinates of Center of Circle of the Pot(Bottom Right)
    pots.push_back(pot1);
    pots.push_back(pot2);
    pots.push_back(pot3);
    pots.push_back(pot4);
}
double get_scale_unit(){
    return 539/box_len;
}
void board::check_board(int x, int y){
    x -= window_width>>1;
    y -= window_length>>1;
    double striker_radius = striker.getRadius();
    Components striker_position = striker.getPosition();
    
    double x1, y1;
    x1 = x/get_scale_unit();
    if (turn == 2){
        y *= (-1);
    }
    y1 = y/get_scale_unit();
    double center_x = striker_position.getX(), center_y = striker_position.getY();
    if (turn == 1){
        center_y*=(-1);
    }
    if ((x1-center_x)*(x1-center_x) + (y1-center_y)*(y1-center_y) <= striker_radius * striker_radius){
        striker.change_state(1);
    }
}

void board::shoot(){
    if (state == IDLE){
        state = ANIMATING;
        double _power = striker.get_power();
        double _angle = striker.get_angle();
        striker.setVelocity(striker_vel * _power * cos(DEG2RAD(_angle)), 
            striker_vel * _power * sin(DEG2RAD(_angle)));
    }
}

void board::move_striker(int x, int y){
    if (striker.getState() == 1){
        x -= window_width>>1;
        y -= window_length>>1;
        double x1;
        x1 = x/get_scale_unit();
        striker.change_state(0);
        double striker_radius = striker.getRadius();
        if (fabs(y) <= (box_len/3 + striker_radius)*get_scale_unit() &&
            fabs(y) >= (box_len/3 - striker_radius)*get_scale_unit()){
            if (x1 >= striker_radius - 2*box_len/5 && x1 + striker_radius <= 2*box_len/5){
                if (turn == 1) {
                    striker.setPosition(x1, -box_len/3);
                }else{
                    striker.setPosition(x1, box_len/3);    
                }
            }
        }
    }
}

void board::DrawBox(double length){
    glPushMatrix();
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glBegin(GL_QUADS);
        glVertex2f(-length, -length);
        glVertex2f(length, -length);
        glVertex2f(length, length);
        glVertex2f(-length, length);
    glEnd();
    glPopMatrix();
}

void board::drawStrikerLines(double x, double y){
    double striker_radius = striker.getRadius();
    glPushMatrix();
        glBegin(GL_LINES);
            glVertex2f(x , y - striker_radius);
            glVertex2f(-x , y - striker_radius);

            glVertex2f(x, y + striker_radius);
            glVertex2f(-x, y + striker_radius);
            
            glVertex2f(x, -y - striker_radius);
            glVertex2f(-x, -y - striker_radius);
            
            glVertex2f(x, -y + striker_radius);
            glVertex2f(-x, -y + striker_radius);

            glVertex2f(x - striker_radius, y);
            glVertex2f(x - striker_radius, -y);

            glVertex2f(x + striker_radius, y);
            glVertex2f(x + striker_radius, -y);

            glVertex2f(-x - striker_radius, y);
            glVertex2f(-x - striker_radius, -y);

            glVertex2f(-x + striker_radius, y);
            glVertex2f(-x + striker_radius, -y);

        glEnd();
    glPopMatrix();
}
void board::draw(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    scorecard.draw(players[turn-1].getScore(), turn);

    //glMatrixMode(GL_MODELVIEW);
    //glLoadIdentity();

    glPushMatrix();
    glTranslatef(0.0f, 0.0f, -8.0f);
    
    glLineWidth(2);
    glColor3f(1.0f, 0.509f, 0.278f);
    // Draw Board
    DrawBox(box_len/2 );
    
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    
    // Draw Striker
    striker.draw();

    unsigned int i=0;
    //Draw Pots
    for(; i < pots.size(); i++){
        pots[i].draw();
    }
    
    //Draw Carrom Mens
    for (i = 0; i<discs.size(); i++){
        discs[i].draw();
    }

    glColor3f(1.0f, 0, 0);
    drawStrikerLines(2 * box_len/5, box_len/3);
    
    double power = striker.get_power();
    if (state == IDLE && power){
        /*Draw the line to decide direction if not in animating state 
        and power!=zero*/
        glPushMatrix();
        double _angle = get_striker_angle();
        Components striker_position = striker.getPosition();
        glColor3f(1.0f, 0.0f, 0.0f);
        glRotatef(_angle, 0.0f, 0.0f, 0.0f);
        glLineWidth(2.2); 
        glBegin(GL_LINES);
            glVertex2f(striker_position.x, striker_position.y);
            glVertex2f(striker_position.x + (0.4+power/2)*cos(DEG2RAD(_angle)), 
                striker_position.y + (0.4 + power/2)*sin(DEG2RAD(_angle)));
        glEnd();
        glPopMatrix();
    }
    glLineWidth(1.0); 
    //Draw Power Bar
    drawPowerBar(1.0f, 2*box_len);



    
    glPopMatrix();
    glutSwapBuffers(); //Using GLUT_DOUBLE
}


void board::move_striker_left(){
    if (state != ANIMATING){
        double new_x = striker.getPosition().x - 0.1f;
        if (new_x >= striker.getRadius() - 2*box_len/5){
            striker.setPositionX(new_x);
        }    
    }    
}

void board::move_striker_right(){
    if (state != ANIMATING){
        double new_x = striker.getPosition().x + 0.1f;
        if (new_x + striker.getRadius() < 2*box_len/5){
            striker.setPositionX(new_x);
        }
    }
}

void board::drawPowerBar(double width, double height){
    double power = striker.get_power();
    glPushMatrix();
    glTranslatef(box_len + 1.2f, -box_len, -8.0f);
    glBegin(GL_QUADS);
    glColor3f(0.0f, 0.0f, 1.0f);
        glVertex2f(0.0f, 0.0f);
        glVertex2f(width, 0.0f);
        glVertex2f(width, height*power);
        glVertex2f(0.0f, height*power);
    glEnd();
    glPopMatrix();
}

int fl = 0, temp, _angle;
void board::advance(double coeff_friction, double dt){
    if (state==ANIMATING){
        state = IDLE;
        unsigned int i = 0, size = discs.size();
        if (striker.advance(coeff_friction, dt)){
            state = ANIMATING;
        }
        for (; i<size; i++){
            if (discs[i].advance(coeff_friction, dt)){
                state = ANIMATING;
            }
        }

        handle_collisions(coeff_friction, dt);

        for (i=0; i<size; i++){
            fl += check_sink(discs[i]);
        }

        temp = check_sink(striker);
        if (state == IDLE){
            if (fl){
                fl = 0;
                if (turn == 1){
                    striker.setPosition(0.0, -2.0);
                }else{
                    striker.setPosition(0.0, 2.0);
                }
            }else{
                if (turn == 2){
                    players[turn-1].angle = striker.get_angle();
                    players[turn-1].power = striker.get_power();
                    turn = 1;
                    striker.setAngle(players[turn-1].angle);
                    striker.setPower(players[turn-1].power);
                    striker.setPosition(0.0, -2.0);
                }
                else{
                    players[turn-1].angle = striker.get_angle();
                    players[turn-1].power = striker.get_power();
                    striker.setAngle(players[turn].angle);
                    striker.setPower(players[turn].power);
                    turn = 2;
                    striker.setPosition(0.0, 2.0);
                }
            }
        }
    }
}

int board::check_sink(disc disk){
    int fl = 0;
    if (disk.getVelocity().getMagnitude() < 20){
        unsigned int i=0, size = pots.size();
        for (;i<size;i++){
            if ((disk.getPosition()-pots[i].getPosition()).getMagnitude() <= 1.6 * (pots[i].getRadius() - disk.getRadius())){
                pocket_disk(disk, i+1);
                fl = 1;
            }
        }
    }
    return fl;
}

void board::pocket_disk(disc pocketed_disk, int pot_no){
    int index=0, size = discs.size(), disk_no = pocketed_disk.getDiskno();
    if (!disk_no){ 
        //Striker out of the map.
        striker.setPosition(1000, 1000);
        //set velocity to zero so that the board can change it's state and turn is changed.
        striker.setVelocity(0, 0);
        //return;
    }else{
        for (;index<size;index++){
            if (discs[index].getDiskno() == disk_no){
                discs.erase(discs.begin()+index);
                break;
            }
        }
    }

    /* Add Pocketed Disk to Pot */
    pots[pot_no-1].add(pocketed_disk);
    /* Add Disk to player*/
    players[turn-1].add_disc(pocketed_disk);
    if (discs.empty()){
        restart();
    }
}

void board::handle_collisions(double coeff_friction, double dt){
    if (state == ANIMATING){
        disc_collisions(coeff_friction, dt);
        wall_collisions(dt);
    }
}

void board::wall_collisions(double dt){
    Components striker_position = striker.getPosition();
    Components striker_velocity = striker.getVelocity();
    double radius = striker.getRadius();
    if(striker_position.x + radius > box_len/2 || striker_position.x - radius < -box_len/2){
        striker.advance(-dt); 
        striker_velocity.x *= -1;
        striker.advance(dt); 
    }
    if(striker_position.y + radius > box_len/2 || striker_position.y - radius < -box_len/2){
        striker.advance(-dt); 
        striker_velocity.y *= -1;    
        striker.advance(dt); 
    }
    striker.setVelocity(striker_velocity);

    for (std::vector<disc> :: iterator iter = discs.begin(); iter < discs.end(); iter++ ){
        Components position = iter->getPosition();  
        Components velocity = iter->getVelocity();
        radius = iter->getRadius();

        if (position.x + radius > box_len/2){
            //Undo the change
            iter->advance(-dt);
            velocity.x = fabs(velocity.x) * -1;
            iter->advance(dt);
        }else if (position.x - radius < -box_len/2){
            iter->advance(-dt);
            velocity.x = fabs(velocity.x);    
            iter->advance(dt);
        }

        if (position.y + radius > box_len/2){
            iter->advance(-dt);
            velocity.y = fabs(velocity.y) * -1;
            iter->advance(dt);
        }else if (position.y - radius < -box_len/2){
            iter->advance(-dt);
            velocity.y = fabs(velocity.y);    
            iter->advance(dt);
        }
        iter->setVelocity(velocity);
    }
}

void board::disc_collisions(double coeff_friction, double dt){
    for(std::vector<disc> :: iterator i = discs.begin(); i < discs.end(); i++)
    {
        if (striker.check_collision_with(*i)){
            striker.handle_collision(*i, coeff_friction, dt);
        }
    }
    for (std::vector<disc> :: iterator i = discs.begin(); i < discs.end(); i++)
    {
        for (std::vector<disc> :: iterator j = i+1; j < discs.end(); j++)
        {
            if (i->check_collision_with(*j)){
                i->handle_collision(*j, coeff_friction, dt);
            }
        }
    }
}

void board::handle_left_click(int state2, int x, int y){
    if (state2 == GLUT_UP){
        if (state!=ANIMATING){
            //Set Angle again if changed again.
            double x1, y1;
            x1 = (x - (window_width>>1))/get_scale_unit();
            y1 = ((window_length>>1) - y)/get_scale_unit();
            Components striker_position = striker.getPosition();
            striker.setAngle(atan2(y1 - striker_position.y, x1 - striker_position.x)*180/PI);    
            if (turn == 1){
                players[turn-1].angle = striker.get_angle();
                players[turn-1].power = striker.get_power();
            }else{
                players[turn-1].angle = striker.get_angle();
                players[turn-1].power = striker.get_power();
            }
            shoot();
        }
    }else if (state2 == GLUT_DOWN && state == IDLE){
        double x1, y1;
        x1 = (x - (window_width>>1))/get_scale_unit();
        y1 = ((window_length>>1) - y)/get_scale_unit();
        Components striker_position = striker.getPosition();
        striker.setAngle(atan2(y1 - striker_position.y, x1 - striker_position.x)*180/PI);    
    }
}

void board::print(){
    for (unsigned int i=0; i<pots.size(); i++){
        cout<<"Pot no. "<<i+1<<":\n";
        pots[i].print();
    }
}