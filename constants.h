#if !defined(MYLIB_CONSTANTS_H)
#define MYLIB_CONSTANTS_H 1

#define DEG2RAD(deg) (deg * PI / 180)
const double ball_rad = 0.13f;
const double pot_rad = 0.17f;
const double striker_rad = ball_rad + 0.01f;
const double box_len = 6.0f;
const double PI = 3.1415926535897932384626433832795;
const double mass_carrom_men = 3.0f;
const double mass_striker = 2 * mass_carrom_men;
const double striker_vel = 6.0f;
double coeff_friction = 0.01f;
const double coeff_restitution = 0.8;
const int no_of_players = 2;
double colors[][3]  = {
    {0, 1, 0},
    {0, 0, 1},
    {1, 0, 0},
    {0.5, 0.5, 0.5},
    {1, 0, 1},
    {0, 1, 1},
    {1, 1, 0},
};
struct timeval tv;
double previous_time = 0.0f;
//float prev_time;
int carrom_men_score[] = {
    30, 20, 50, -10
};
int type_of_carrom_men = 4;
#endif